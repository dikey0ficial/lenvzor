package main

import (
	"gioui.org/app"
	"gioui.org/unit"
	"github.com/ncruces/zenity"
	"log"
	"os"
)

var (
	debl, errl *log.Logger
)

func init() {
	debl = log.New(os.Stdout, "[DEBUG]\t", log.Ldate|log.Ltime|log.Lshortfile)
	errl = log.New(os.Stderr, "[ERROR]\t", log.Ldate|log.Ltime|log.Lshortfile)
}

func work(ui *UI) {
	defer func() {
		if e := recover(); e != nil {
			errl.Println(e)
			zenity.Error("Случилась фатальная ошибка", zenity.Title("Ошибка"))
			os.Exit(1)
		}
	}()
	fsize := [2]unit.Value{unit.Dp(768), unit.Dp(512)}
	options := []app.Option{
		app.Title("Ленвзор"),
		app.Size(fsize[0], fsize[1]),
		app.MinSize(fsize[0], fsize[1]),
	}
	w := app.NewWindow(options...)
	if err := ui.Run(w); err != nil {
		errl.Println(err)
		zenity.Error("Ошибка: "+err.Error(), zenity.Title("Ошибка"))
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}

func main() {
	ui := NewUI()
	go work(ui)
	app.Main()
}
