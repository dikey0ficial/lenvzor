package main

import (
	"gioui.org/widget"
	"gocv.io/x/gocv"
	"image"
	"image/color"
)

func checkForSubmit(events []widget.EditorEvent) bool {
	for _, ev := range events {
		if _, ok := ev.(widget.SubmitEvent); ok {
			return true
		}
	}
	return false
}

func getFrameFromMat(mat gocv.Mat) []byte {
	if mat.Channels() < 3 {
		return []byte{}
	}
	var (
		frame = make([]byte, 0, mat.Rows()*mat.Cols()*3)
		vecb  gocv.Vecb
	)
	for y := 0; y < mat.Rows(); y++ {
		for x := 0; x < mat.Cols(); x++ {
			vecb = mat.GetVecbAt(y, x)
			for c := 2; c >= 0; c-- {
				frame = append(frame, vecb[c])
			}
		}
	}
	return frame
}

func swapBGRandRGB(frame []byte) []byte {
	var (
		result = make([]byte, len(frame))
	)
	for i := 0; i < len(frame); i += 3 {
		result[i] = frame[i+2]
		result[i+1] = frame[i+1]
		result[i+2] = frame[i]
	}
	return result
}

type EmptyImage struct{}

// ColorModel implements image.Image and returns color model of image
func (_ EmptyImage) ColorModel() color.Model {
	return color.NRGBAModel
}

// Bounds implements image.Image interface and returns (0; 0)
func (_ EmptyImage) Bounds() image.Rectangle {
	return image.Rectangle{
		image.Point{0, 0},
		image.Point{0, 0},
	}
}

// At implements image.Image inteface and returns color of (x; y)
func (_ EmptyImage) At(x, y int) color.Color {
	return color.NRGBA{}
}

type Error string

func (e Error) Error() string { return string(e) }

const (
	ErrNilVideo = Error("video is nil or empty")
)
