package main

import (
	"gioui.org/layout"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget"
	"github.com/AlexEidt/Vidio"
	"gocv.io/x/gocv"
	"image"
	"image/color"
)

type IMat struct {
	askedForMat     bool
	processed       bool
	customVideoPath string
	buffer          [][]byte

	Op    paint.ImageOp
	Mat   gocv.Mat
	Video *vidio.Video
}

// NewIMat returns new empty IMat
func NewIMat() *IMat {
	im := &IMat{
		Op:    paint.ImageOp{},
		Mat:   gocv.NewMat(),
		Video: nil,
	}
	return im
}

func (im *IMat) SetMat(mat gocv.Mat) {
	im.Mat = mat
	im.Op = paint.NewImageOp(im)
	if im.Video != nil {
		if im.buffer == nil {
			im.buffer = make([][]byte, 0)
		}
		frame := getFrameFromMat(mat)
		debl.Println(len(frame))
		im.buffer = append(im.buffer, frame)
	}
}

func (im *IMat) SetVideo(vid *vidio.Video) error {
	im.Video = vid
	im.askedForMat = false
	if !im.Video.Read() {
		return ErrNilVideo
	}
	frame := vid.FrameBuffer()
	mat, err := gocv.NewMatFromBytes(vid.Height(), vid.Width(), gocv.MatTypeCV8UC3, swapBGRandRGB(frame))
	im.Mat = mat
	im.Op = paint.NewImageOp(im)
	return err
}

func (im *IMat) GetFrameBuffers() [][]byte { return im.buffer }
func (im *IMat) SetProcessed(b bool)       { im.processed = b }
func (im *IMat) GetProcessed() bool        { return im.processed }
func (im *IMat) ResetProcess()             { im.askedForMat = false }
func (im *IMat) SetFilename(name string)   { im.customVideoPath = name }

func (im *IMat) NextMat() bool {
	res := !im.askedForMat
	im.askedForMat = true
	if im.Video != nil {
		if im.askedForMat {
			res = im.Video.Read()
		}
		var (
			mat gocv.Mat
			err error
		)
		if res {
			mat, err = gocv.NewMatFromBytes(im.Video.Height(), im.Video.Width(), gocv.MatTypeCV8UC3, swapBGRandRGB(im.Video.FrameBuffer()))
			im.Mat = mat
		}
		return res && err == nil
	}
	return res
}

// ColorModel implements image.Image and returns color model of image
func (im *IMat) ColorModel() color.Model {
	return color.NRGBAModel
}

// Bounds implements image.Image interface and returns bounds of mat
func (im *IMat) Bounds() image.Rectangle {
	return image.Rectangle{
		image.Point{0, 0},
		image.Point{im.Mat.Cols() - 1, im.Mat.Rows() - 1},
	}
}

// At implements image.Image inteface and returns color of (x; y)
// Let's hope that there will be only BGR(A) matrices...
func (im *IMat) At(x, y int) color.Color {
	vec := im.Mat.GetVecbAt(y, x)
	if len(vec) < 3 {
		return color.NRGBA{}
	}
	var a byte = 255
	if len(vec) >= 4 {
		a = vec[3]
	}
	return color.NRGBA{
		R: vec[2],
		G: vec[1],
		B: vec[0],
		A: a,
	}
}

// Layout layouts IMat as widget
func (im *IMat) Layout(gtx C, th T) D {
	if s := im.Op.Size(); s.X == 0 || s.Y == 0 {
		return D{}
	}
	var scale float32
	if s := gtx.Constraints.Max; s.Y < s.X {
		scale = float32(s.Y)
	} else {
		scale = float32(s.X)
	}
	for scale > 1 {
		scale /= 10
	}
	img := widget.Image{
		Src:      im.Op,
		Fit:      widget.ScaleDown,
		Position: layout.W,
		Scale:    0,
	}
	return widget.Border{
		Color:        th.ContrastBg,
		Width:        unit.Dp(2.5),
		CornerRadius: unit.Dp(5),
	}.Layout(gtx, func(gtx C) D {
		return img.Layout(gtx)
	})
}
