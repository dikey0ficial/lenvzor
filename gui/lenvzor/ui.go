package main

import (
	"bytes"
	_ "embed"
	"gioui.org/app"
	"gioui.org/font/gofont"
	"gioui.org/io/system"
	"gioui.org/layout"
	"gioui.org/op"
	"gioui.org/op/paint"
	"gioui.org/unit"
	"gioui.org/widget"
	"gioui.org/widget/material"
	"github.com/AlexEidt/Vidio"
	"github.com/ncruces/zenity"
	"gocv.io/x/gocv"
	"image"
	"image/color"
	"image/png"
	"path/filepath"
	"strings"
)

var (
	accentColor = color.NRGBA{R: 95, G: 201, B: 157, A: 255} // #5FC99D

	//go:embed icon_with_text.png
	iconWithTextBytes []byte
)

// type aliases
type (
	C = layout.Context
	D = layout.Dimensions
	T = *material.Theme
)

type UI struct {
	classifier       gocv.CascadeClassifier
	classifierLoaded bool
	processing       bool
	frameCh          chan struct{}
	textActionText   string

	Theme                *material.Theme
	IconWithText         image.Image
	ModeChooser          *Chooser
	SaveAudioCheckbox    material.CheckBoxStyle
	LoadButton           material.ButtonStyle
	LoadClassifierButton material.ButtonStyle
	ProcessButton        material.ButtonStyle
	SaveButton           material.ButtonStyle
	ActionChooser        *Chooser
	TextActionEditor     material.EditorStyle
	TextActionButton     material.ButtonStyle
	Media                *MediaActivity
}

func NewUI() *UI {
	ui := new(UI)
	ui.Theme = material.NewTheme(gofont.Collection())
	ui.Theme.ContrastBg = accentColor
	img, err := png.Decode(bytes.NewReader(iconWithTextBytes))
	if err != nil {
		img = EmptyImage{}
	}
	ui.IconWithText = img
	modeGroup := new(widget.Enum)
	modeGroup.Value = "photo"
	ui.ModeChooser = &Chooser{
		"Источник:",
		modeGroup,
		[]material.RadioButtonStyle{
			material.RadioButton(
				ui.Theme,
				modeGroup,
				"photo",
				"С картинки",
			),
			material.RadioButton(
				ui.Theme,
				modeGroup,
				"video",
				"С видео",
			),
		},
	}
	ui.SaveAudioCheckbox = material.CheckBox(
		ui.Theme,
		new(widget.Bool),
		"Сохранять аудио",
	)
	ui.LoadClassifierButton = material.Button(
		ui.Theme,
		new(widget.Clickable),
		"Загрузить XML классификатора",
	)
	ui.ProcessButton = material.Button(
		ui.Theme,
		new(widget.Clickable),
		"Обработать",
	)
	ui.LoadButton = material.Button(
		ui.Theme,
		new(widget.Clickable),
		"Загрузить",
	)
	ui.SaveButton = material.Button(
		ui.Theme,
		new(widget.Clickable),
		"Сохранить",
	)
	actionGroup := new(widget.Enum)
	actionGroup.Value = "blur"
	ui.ActionChooser = &Chooser{
		"Режим работы:",
		actionGroup,
		[]material.RadioButtonStyle{
			material.RadioButton(
				ui.Theme,
				actionGroup,
				"blur",
				"Размытие",
			),
			material.RadioButton(
				ui.Theme,
				actionGroup,
				"square",
				"Выделение квадратом",
			),
			material.RadioButton(
				ui.Theme,
				actionGroup,
				"put_text",
				"Пометка текстом",
			),
		},
	}
	ui.Media = new(MediaActivity)
	ui.Media.Original = NewIMat()
	ui.Media.Result = NewIMat()
	imList := new(widget.List)
	imList.List.Axis = layout.Horizontal
	ui.Media.List = material.List(ui.Theme, imList)
	ui.TextActionEditor = material.Editor(
		ui.Theme,
		&widget.Editor{SingleLine: true, Submit: true},
		"Введите текст, которым будет помечаться",
	)
	ui.TextActionButton = material.Button(
		ui.Theme,
		new(widget.Clickable),
		"Задать текст",
	)
	ui.classifier = gocv.NewCascadeClassifier()
	ui.frameCh = make(chan struct{})
	ui.textActionText = "*"
	ui.TextActionEditor.Editor.SetText(ui.textActionText)
	return ui
}

func (ui *UI) Run(w *app.Window) error {
	var (
		ops op.Ops
		err error
	)
CHANNEL_FOR:
	for {
		select {
		case e := <-w.Events():
			switch e := e.(type) {
			case system.FrameEvent:
				gtx := layout.NewContext(&ops, e)
				ui.Layout(gtx)
				e.Frame(gtx.Ops)
			case system.DestroyEvent:
				err = e.Err
				break CHANNEL_FOR
			}
		case <-ui.frameCh:
			w.Invalidate()
		}
	}
	return err
}

func (ui *UI) Layout(gtx C) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(
			func(gtx C) D {
				return layout.Center.Layout(gtx,
					widget.Image{
						Src:   paint.NewImageOp(ui.IconWithText),
						Scale: 0.5,
					}.Layout,
				)

			},
		),
		layout.Rigid(
			func(gtx C) D {
				if ui.ModeChooser.Group.Value == "video" {
					return layout.Flex{
						Axis: layout.Horizontal,
					}.Layout(gtx,
						layout.Rigid(func(gtx C) D {
							return ui.ModeChooser.Layout(gtx, ui.Theme)
						}),
						layout.Rigid(ui.SaveAudioCheckbox.Layout),
					)
				}
				return ui.ModeChooser.Layout(gtx, ui.Theme)
			},
		),
		layout.Rigid(
			func(gtx C) D {
				return ui.ActionChooser.Layout(gtx, ui.Theme)
			},
		),
		layout.Rigid(func(gtx C) D {
			if ui.ActionChooser.Group.Value != "put_text" {
				return D{}
			}
			if ui.TextActionEditor.Editor.Len() > 64 {
				ui.TextActionEditor.Editor.Delete(64 - ui.TextActionEditor.Editor.Len())
			}
			if ui.TextActionButton.Button.Clicked() || checkForSubmit(ui.TextActionEditor.Editor.Events()) {
				if ui.TextActionEditor.Editor.Len() == 0 {
					zenity.Error("Ошибка: пустое поле", zenity.Title("Ошибка"))
				} else {
					txt := strings.TrimSpace(ui.TextActionEditor.Editor.Text())
					if len(txt) == 0 {
						zenity.Error("Ошибка: пустое поле", zenity.Title("Ошибка"))
					} else {
						ui.textActionText = txt
					}

				}
			}
			return layout.Flex{
				Axis: layout.Horizontal,
			}.Layout(gtx,
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(func(gtx C) D {
					gtx.Constraints.Min.Y += 15
					return widget.Border{
						Width:        unit.Dp(1),
						Color:        color.NRGBA{A: 255},
						CornerRadius: unit.Dp(5),
					}.Layout(gtx, func(gtx C) D {
						return layout.UniformInset(unit.Dp(7.5)).Layout(gtx, ui.TextActionEditor.Layout)
					})
				}),
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(ui.TextActionButton.Layout),
			)
		}),
		layout.Rigid(layout.Spacer{Height: unit.Dp(5)}.Layout),
		layout.Rigid(func(gtx C) D {
			return layout.Flex{Axis: layout.Horizontal}.Layout(gtx,
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(func(gtx C) D {
					if mode := ui.ModeChooser.Group.Value; mode == "photo" {
						ui.LoadButton.Text = "Загрузить картинку"
					} else if mode == "video" {
						ui.LoadButton.Text = "Загрузить видео"
					}
					if ui.LoadButton.Button.Clicked() {
						var mstring = "картинки"
						if ui.ModeChooser.Group.Value == "video" {
							mstring = "видео"
						}
						fileFilters := make(zenity.FileFilters, 0)
						if mode := ui.ModeChooser.Group.Value; mode == "photo" {
							fileFilters = zenity.FileFilters{
								{Name: "PNG", Patterns: []string{"*.png"}},
								{Name: "JPEG", Patterns: []string{"*.jpg", "*.jpeg"}},
								{Name: "BMP", Patterns: []string{"*.bmp"}},
								{Name: "TIFF", Patterns: []string{"*.tiff"}},
								{Name: "WebP", Patterns: []string{"*.webp"}},
							}
						} else if mode == "video" {
							fileFilters = zenity.FileFilters{
								{Name: "MP4", Patterns: []string{"*.mp4"}},
								{Name: "AVI", Patterns: []string{"*.avi"}},
								{Name: "Matroshka", Patterns: []string{"*.mkv"}},
								{Name: "GIF", Patterns: []string{"*.gif"}},
								{Name: "WebM", Patterns: []string{"*.webm"}},
								{Name: "Theora (ogv)", Patterns: []string{"*.ogv"}},
								{Name: "Theora (ogg)", Patterns: []string{"*.ogg"}},
								{Name: "FLV", Patterns: []string{"*.flv"}},
								{Name: "MOV", Patterns: []string{"*.mov"}},
								{Name: "3GP", Patterns: []string{"*.3gp"}},
								{Name: "MPEG", Patterns: []string{"*.mpeg"}},
							}
						}
						filename, err := zenity.SelectFile(
							zenity.Title("Сохранение  "+mstring),
							fileFilters,
						)
						if err == nil {
							if mode := ui.ModeChooser.Group.Value; mode == "photo" {
								ui.processing = true
								go func() {
									mat := gocv.IMRead(filename, gocv.IMReadUnchanged)
									if mat.Empty() {
										zenity.Error("Ошибка при загрузке картинки", zenity.Title("Ошибка"))
									} else {
										ui.Media.Original.SetMat(mat)
										ui.Media.Result = NewIMat()
									}
									ui.processing = false
									ui.frameCh <- struct{}{}
								}()
							} else if mode == "video" {
								ui.processing = true
								go func() {
									vid, err := vidio.NewVideo(filename)
									if err != nil {
										errl.Println(err)
										zenity.Error("Ошибка при загрузке видео", zenity.Title("Ошибка"))
									} else {
										ui.Media.Original.SetVideo(vid)
										ui.Media.Result = NewIMat()
									}
									ui.processing = false
									ui.frameCh <- struct{}{}
								}()
							}
						}
					}
					return ui.LoadButton.Layout(gtx)
				}),
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(
					func(gtx C) D {
						if ui.LoadClassifierButton.Button.Clicked() {
							fpath, err := zenity.SelectFile(zenity.Title("Загрузка классификатора"), zenity.FileFilters{{Name: "XML", Patterns: []string{"*.xml"}}})
							if err == nil {
								if !ui.classifier.Load(fpath) {
									zenity.Error("Ошибка при загрузке классификатора", zenity.Title("Ошибка"))
									ui.LoadClassifierButton.Text = "Загрузить XML классификатора"
								} else {
									text := filepath.Base(fpath)
									if len([]rune(text)) > 24 {
										text = string([]rune(text)[:21]) + "..."
									}
									ui.LoadClassifierButton.Text = text
									ui.classifierLoaded = true
								}
							}
						}
						return ui.LoadClassifierButton.Layout(gtx)
					},
				),
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(
					func(gtx C) D {
						if ui.ProcessButton.Button.Clicked() {
							if ui.Media.Original.Mat.Empty() {
								zenity.Error("Картинка или видео не загружена/о", zenity.Title("Ошибка"))
							} else if !ui.classifierLoaded {
								zenity.Error("Классификатор не загружен", zenity.Title("Ошибка"))
							} else {
								ui.processing = true
								go func() {
									defer func() {
										ui.processing = false
										ui.frameCh <- struct{}{}
									}()
									if ui.Media.Original.Video != nil {
										if ui.Media.Original.GetProcessed() {
											var err error
											ui.Media.Original.Video, err = vidio.NewVideo(ui.Media.Original.Video.FileName())
											if err != nil {
												zenity.Error("Ошибка при обработке видео", zenity.Title("Ошибка"))
												return
											}
										} else {
											ui.Media.Result = NewIMat()
											ui.Media.Result.Video = new(vidio.Video)
											ui.Media.Original.SetProcessed(true)
										}
									} else {
										ui.Media.Original.ResetProcess()
									}
									for ui.Media.Original.NextMat() {
										rects := ui.classifier.DetectMultiScale(ui.Media.Original.Mat)
										var mat = gocv.NewMat()
										ui.Media.Original.Mat.CopyTo(&mat)
										debl.Println(len(rects))
										for _, rect := range rects {
											debl.Println(rect)
											switch ui.ActionChooser.Group.Value {
											case "blur":
												reg := mat.Region(rect)
												gocv.GaussianBlur(reg, &reg, image.Pt(175, 175), 0, 0, gocv.BorderDefault)
												reg.Close()
											case "square":
												gocv.Rectangle(&mat, rect, color.RGBA{A: 255, R: 255}, 1)
											case "put_text":
												gocv.PutText(&mat, ui.textActionText, rect.Max, gocv.FontHersheySimplex, 5, color.RGBA{A: 255, R: 255, G: 255, B: 255}, 15)
											}
										}
										ui.Media.Result.SetMat(mat)
									}
								}()
							}
						}
						return ui.ProcessButton.Layout(gtx)
					},
				),
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(func(gtx C) D {
					if mode := ui.ModeChooser.Group.Value; mode == "photo" {
						ui.SaveButton.Text = "Сохранить картинку"
					} else if mode == "video" {
						ui.SaveButton.Text = "Сохранить видео"
					}
					if ui.SaveButton.Button.Clicked() {
						if ui.Media.Result.Mat.Empty() {
							zenity.Error("Ошибка: картинка или видео ещё не была/о обработана/о", zenity.Title("Ошибка"))
						} else {
							var mstring = "картинки"
							if ui.ModeChooser.Group.Value == "video" {
								mstring = "видео"
							}
							fileFilters := make(zenity.FileFilters, 0)
							if mode := ui.ModeChooser.Group.Value; mode == "photo" {
								fileFilters = zenity.FileFilters{
									{Name: "PNG", Patterns: []string{"*.png"}},
									{Name: "JPG", Patterns: []string{"*.jpg", "*.jpeg"}},
									{Name: "BMP", Patterns: []string{"*.bmp"}},
									{Name: "TIFF", Patterns: []string{"*.tiff"}},
									{Name: "WebP", Patterns: []string{"*.webP"}},
								}
							} else if mode == "video" {
								fileFilters = zenity.FileFilters{
									{Name: "MP4", Patterns: []string{"*.mp4"}},
									{Name: "AVI", Patterns: []string{"*.avi"}},
									{Name: "Matroshka", Patterns: []string{"*.mkv"}},
									{Name: "GIF", Patterns: []string{"*.gif"}},
									{Name: "WebM", Patterns: []string{"*.webm"}},
									{Name: "Theora (ogv)", Patterns: []string{"*.ogv"}},
									{Name: "Theora (ogg)", Patterns: []string{"*.ogg"}},
									{Name: "FLV", Patterns: []string{"*.flv"}},
									{Name: "MOV", Patterns: []string{"*.mov"}},
									{Name: "3GP", Patterns: []string{"*.3gp"}},
									{Name: "MPEG", Patterns: []string{"*.mpeg"}},
								}
							}
							filename, err := zenity.SelectFileSave(
								zenity.Title("Сохранение  "+mstring),
								fileFilters,
							)
							if err == nil {
								if mode := ui.ModeChooser.Group.Value; mode == "photo" {
									ui.processing = true
									go func() {
										ok := gocv.IMWrite(filename, ui.Media.Result.Mat)
										if !ok {
											zenity.Error("Произошла ошибка при сохранении картинки", zenity.Title("Ошибка"))
										}
										ui.processing = false
										ui.frameCh <- struct{}{}
									}()
								} else if mode == "video" {
									ui.processing = true
									go func() {
										defer func() {
											ui.processing = false
											ui.frameCh <- struct{}{}
										}()
										debl.Println(filename)
										options := vidio.Options{
											FPS:     ui.Media.Original.Video.FPS(),
											Bitrate: ui.Media.Original.Video.Bitrate(),
										}
										if ui.SaveAudioCheckbox.CheckBox.Value {
											options.Audio = ui.Media.Original.Video.FileName()
										}
										writer, err := vidio.NewVideoWriter(
											filename,
											ui.Media.Original.Video.Width(),
											ui.Media.Original.Video.Height(),
											&options,
										)
										if err != nil {
											errl.Println(err)
											zenity.Error("Произошла ошибка при сохранении видео", zenity.Title("Ошибка"))
										} else {

											defer writer.Close()
											for _, frame := range ui.Media.Result.GetFrameBuffers() {
												debl.Println("frame_len", len(frame))
												err = writer.Write(frame)
												if err != nil {
													errl.Println(err)
													zenity.Error("Произошла ошибка при сохранении видео", zenity.Title("Ошибка"))
													break
												}
											}
											if err == nil {
												ui.Media.Result.SetFilename(filename)
											}
										}

									}()
								}
							}
						}
					}
					return ui.SaveButton.Layout(gtx)
				}),
				layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
				layout.Rigid(func(gtx C) D {
					if ui.processing {
						return material.Loader(ui.Theme).Layout(gtx)
					}
					return D{}
				}),
			)
		}),
		layout.Rigid(func(gtx C) D {
			return ui.Media.Layout(gtx, ui.Theme)
		}),
	)
}

type Chooser struct {
	Label    string
	Group    *widget.Enum
	RButtons []material.RadioButtonStyle
}

func (c *Chooser) Layout(gtx C, th T) D {
	if len(c.RButtons) == 0 || c.Group == nil {
		return material.Label(th, unit.Dp(15), "ошибка при загрузке элемента").Layout(gtx)
	}
	childs := make([]layout.FlexChild, 0, (len(c.RButtons)+1)*2+1)
	childs = append(
		childs,
		layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
		layout.Rigid(material.Label(th, unit.Dp(25), c.Label).Layout),
		layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
	)
	for _, rb := range c.RButtons {
		childs = append(
			childs,
			layout.Rigid(rb.Layout),
			layout.Rigid(layout.Spacer{Width: unit.Dp(5)}.Layout),
		)
	}
	return layout.Flex{
		Axis: layout.Horizontal,
	}.Layout(gtx,
		childs...,
	)
}

type MediaActivity struct {
	List     material.ListStyle
	Original *IMat
	Result   *IMat
}

func (m *MediaActivity) Layout(gtx C, th T) D {
	return layout.Flex{
		Axis: layout.Vertical,
	}.Layout(gtx,
		layout.Rigid(layout.Spacer{Height: unit.Dp(15)}.Layout),
		layout.Rigid(func(gtx C) D {
			return m.List.Layout(
				gtx, 2, func(gtx C, index int) D {
					var im *IMat
					switch index {
					case 0:
						im = m.Original
					case 1:
						im = m.Result
					default:
						return D{}
					}
					return layout.Flex{
						Axis: layout.Horizontal,
					}.Layout(gtx,
						layout.Rigid(func(gtx C) D { return im.Layout(gtx, th) }),
						layout.Rigid(layout.Spacer{Width: unit.Dp(25)}.Layout),
					)
				},
			)
		}),
	)
}
